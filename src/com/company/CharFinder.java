package com.company;

public class CharFinder {
    public static int find (char [] chArr, char x, int ucs, int uce, int lcs, int lce){
        if(Character.isUpperCase(x)){
            for(int i=ucs; i<=uce; i++){
                if(chArr[i]==x) return i;
            }
        }
        if(Character.isLowerCase(x)){
            for(int i=lcs; i<=lce; i++){
                if(chArr[i]==x) return i;
            }
        }
        return -1;
    }
}
