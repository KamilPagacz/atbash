package com.company;

class AtBashCipher implements Cipher {

    public static int UpperCaseStartIndex = 0;
    public static int UpperCaseEndIndex = 25;
    public static int LowerCaseStartIndex = 26;
    public static int LowerCaseEndIndex = 51;

    private static final char [] alphabet = {
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'
    };

    public String AtBashCipherEncoderDecoder(String input){
        StringBuilder output = new StringBuilder();
        int index;
        for (int i=0; i<input.length(); i++) {
            index = CharFinder.find(alphabet,input.charAt(i), UpperCaseStartIndex, UpperCaseEndIndex, LowerCaseStartIndex, LowerCaseEndIndex);
            if(index>= UpperCaseStartIndex && index<= UpperCaseEndIndex){
                output.append(Character.toUpperCase(alphabet[UpperCaseEndIndex -index]));
            }
            else if(index>= LowerCaseStartIndex && index<= LowerCaseEndIndex){
                output.append(Character.toLowerCase(alphabet[LowerCaseEndIndex -index]));
            }
            else{
                output.append(input.charAt(i));
            }
        }
        return output.toString();
    }

    @Override
    public String encode(String message){
        return AtBashCipherEncoderDecoder(message);
    }

    @Override
    public String decode(String message) {
        return AtBashCipherEncoderDecoder(message);
    }

}
